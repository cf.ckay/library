package com.gitlab.cf.ckay;

import java.util.ArrayList;
import java.util.List;

public class MainBook {
    static final List<Book> books = new ArrayList<>();

    public static void main(String[] args) {
        books.add(new Book("First book", 2));

        for (int i = 0; i < 5; i++) {
            new Thread(new Reader(books.get(0)), "Thread" + i).start();
        }
    }
}