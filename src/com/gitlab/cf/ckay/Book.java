package com.gitlab.cf.ckay;

public class Book {
    private String name;
    private volatile int amount;
    private final int countBooks;

    public Book(String name, int amount) {
        if (amount < 1) {
            throw new IllegalArgumentException("Amount = " + amount);
        }

        this.name = name;
        this.amount = amount;
        this.countBooks = amount;
    }

    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;

        if (this.amount < 0 || this.amount > countBooks) {
            throw new IllegalArgumentException("Amount = " + this.amount);
        }
    }
}