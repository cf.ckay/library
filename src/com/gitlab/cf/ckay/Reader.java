package com.gitlab.cf.ckay;

import static java.lang.Thread.sleep;

public class Reader implements Runnable {
    private Book book;

    public Reader(Book book) {
        this.book = book;
    }

    @Override
    public void run() {
        try {
            readBook(book);
            sleep(1000);
            comebackBook(book);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void comebackBook(Book book) {
        synchronized (book) {
            book.setAmount(book.getAmount() + 1);
            System.out.println(Thread.currentThread().getName() + " finishing reading.\nAvailable of books - " + book.getAmount());
            book.notify();
        }
    }

    public void readBook(Book book) throws InterruptedException {
        synchronized (book) {
            while (book.getAmount() == 0) {
                System.out.println(Thread.currentThread().getName() + " is waiting...");
                book.wait();
            }
            book.setAmount(book.getAmount() - 1);
            System.out.println(Thread.currentThread().getName() + " is reading the book!\nAvailable of books - " + book.getAmount());
        }
    }
}